Hi there :wave:

I'm Vlad, and I'm an Engineering Manager on the Knowledge team at GitLab. My team owns:
- GitLab Wiki
- GitLab Pages
- everything about markdown and rich text editor
- GLQL

I focus on people over processes, strongly believe in DRI's and people being managers of one.

Read about what my team does [here](https://gitlab.com/gitlab-org/plan-stage/knowledge-group/-/wikis/Weekly-updates).

## How to work with me

- I only get notifications is you tag me as `@vshushlin` because I use GitLab TODOs as my inbox.

## Personal

My [personal development plan](https://gitlab.com/vshushlin/personal-development/-/wikis/Home) is public.

- I currently live in Amsterdam, Netherlands. I moved in the summer of 2022, and still figuring out the local ways of doing things. 🇳🇱
- 🚴🏻 I recently started biking almost daily!
- I do like to use emojies a lot, so expect to see smiley faces very often 😉😍💚
- My favorite thing about working at GitLab is working with community contributors.
    I enjoyed it a lot while working on the small GitLab Pages project,
    and I’m super excited about bringing more contributions to the bigger GitLab project.